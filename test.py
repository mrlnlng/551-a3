import cupy as np
from main import Dense, Activation, ACTIVATIONS, Loss

# xor problem
inputs = np.array([
   [1,1],
   [1,0],
   [0,0],
   [1,1]
])
outputs = np.array([
    [0,1],
    [0,1],
    [0,1],
    [1,0]
])
softmax = Activation("softmax")
real_inputs = softmax.forward(inputs)
real_outputs = softmax.forward(outputs)

loss = Loss("crossentropy")
res = loss.forward(real_inputs,outputs)
grads = loss.backward(real_inputs, outputs)

print(res)
print(grads)
